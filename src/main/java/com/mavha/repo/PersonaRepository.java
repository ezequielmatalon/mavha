package com.mavha.repo;

import com.mavha.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonaRepository extends JpaRepository<Person, Long> {
	Optional<Person> findOneById(Long id);
}

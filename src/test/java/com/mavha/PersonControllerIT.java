package com.mavha;

import com.mavha.dto.PersonRequest;
import com.mavha.dto.PersonResponse;
import java.util.List;
import java.util.Random;
import org.dummycreator.DummyCreator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonControllerIT {

  @Autowired
  private TestRestTemplate restTemplate;

  private DummyCreator dummyCreator;

  @Before
  public void init() {
    dummyCreator = new DummyCreator();
  }

  @Test
  public void listPerson() {
    ResponseEntity<List> persons = restTemplate.getForEntity("/api/person", List.class);
    Assert.assertTrue(!persons.getBody().isEmpty());
  }


  @Test
  public void createPerson() {
    PersonResponse response = restTemplate.postForObject("/api/person/1",
        personRequest(), PersonResponse.class);
    Assert.assertEquals(Long.valueOf(1L), response.getId());
    Assert.assertTrue(response.getLastName() != null);
    Assert.assertTrue(response.getAge() != null);
    Assert.assertTrue(response.getName() != null);
    Assert.assertTrue(!response.getName().isEmpty());
    Assert.assertTrue(!response.getLastName().isEmpty());
    Assert.assertTrue(response.getAge().intValue() > 0);
  }

  @Test
  public void existingPerson() {
    PersonRequest personRequest = personRequest();
    Long id = randomId();
    ResponseEntity<PersonResponse> response1 = restTemplate.postForEntity("/api/person/" + id,
        personRequest, PersonResponse.class);
    ResponseEntity<PersonResponse> response2 = restTemplate.postForEntity("/api/person/" + id,
        personRequest, PersonResponse.class);
    Assert.assertEquals(HttpStatus.OK.value(), response1.getStatusCodeValue());
    Assert.assertEquals(400, response2.getStatusCodeValue());

  }

  @Test
  public void nullAge() {
    PersonRequest personRequest = personRequest();
    personRequest.setAge(null);
    ResponseEntity<List> response1 = restTemplate.postForEntity("/api/person/" + randomId(), personRequest, List.class);
    Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response1.getStatusCodeValue());
  }

  @Test
  public void negativeAge() {
    PersonRequest personRequest = personRequest();
    personRequest.setAge(-1);
    ResponseEntity<List> response1 = restTemplate.postForEntity("/api/person/" + randomId(), personRequest, List.class);
    Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response1.getStatusCodeValue());
  }

  @Test
  public void nullName() {
    PersonRequest personRequest = personRequest();
    personRequest.setName(null);
    ResponseEntity<List> response1 = restTemplate.postForEntity("/api/person/" + randomId(), personRequest, List.class);
      System.out.println(response1.getStatusCodeValue());
    Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response1.getStatusCodeValue());
  }

  @Test
  public void emptyName() {
    PersonRequest personRequest = personRequest();
    personRequest.setName("");
    ResponseEntity<List> response1 = restTemplate.postForEntity("/api/person/" + randomId(), personRequest, List.class);
    Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response1.getStatusCodeValue());
  }

  @Test
  public void nullLastName() {
    PersonRequest personRequest = personRequest();
    personRequest.setLastName(null);
    ResponseEntity<List> response1 = restTemplate.postForEntity("/api/person/" + randomId(), personRequest, List.class);
    Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response1.getStatusCodeValue());
  }

  @Test
  public void emptyLastName() {
    PersonRequest personRequest = personRequest();
    personRequest.setLastName("");
    ResponseEntity<List> response1 = restTemplate.postForEntity("/api/person/" + randomId(), personRequest, List.class);
    Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), response1.getStatusCodeValue());
  }

  @Test
  public void emptyId() {
    PersonRequest personRequest = personRequest();
    ResponseEntity<String> response1 = restTemplate.postForEntity("/api/person", personRequest, String.class);
    Assert.assertEquals(405, response1.getStatusCodeValue());
  }

  private PersonRequest personRequest() {
    PersonRequest personRequest = dummyCreator.create(PersonRequest.class);
    personRequest.setAge(randomAge());
    return personRequest;
  }

  private Long getRandom(Long min, Long max) {
    return new Random().longs(min, max).findFirst().getAsLong();
  }

  private Integer getRandom(Integer min, Integer max) {
    return new Random().ints(min, max).findFirst().getAsInt();
  }

  private Integer randomAge() {
    return getRandom(1, 150);
  }

  private Long randomId() {
    return getRandom(2l, 99999999l);
  }

}

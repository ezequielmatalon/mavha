package com.mavha.controller.impl;

import com.mavha.controller.PersonController;
import com.mavha.dto.PersonRequest;
import com.mavha.dto.PersonResponse;
import com.mavha.model.Person;
import com.mavha.service.PersonService;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonControllerImpl implements PersonController {

    @Autowired
    private PersonService personService;
    @Autowired
    private MapperFacade orikaMapper;

    @Override
    public List<PersonResponse> readAllPersons() {
        return orikaMapper.mapAsList(personService.getAll(), PersonResponse.class);
    }

    @Override
    public PersonResponse createPerson(@NotNull Long id, @NotNull @Valid PersonRequest personRequest) {
        Person person = orikaMapper.map(personRequest, Person.class);
        person.setId(id);
        return orikaMapper.map(personService.create(person), PersonResponse.class);
    }

}

package com.mavha.service.impl;

import com.mavha.exception.PersonAlreadyExist;
import com.mavha.model.Person;
import com.mavha.repo.PersonaRepository;
import com.mavha.service.PersonService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

  @Autowired
  private PersonaRepository personaRepository;
  private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

  @Override
  public List<Person> getAll() {
    return personaRepository.findAll();
  }

  @Override
  public Person create(Person person) {
    Optional<Person> optPersona = personaRepository.findOneById(person.getId());
    if (optPersona.isPresent()) {
      LOGGER.error(String.format("Person already exists with id=%d", person.getId()));
      throw new PersonAlreadyExist(person.getId());
    }
    return personaRepository.save(person);
  }
}

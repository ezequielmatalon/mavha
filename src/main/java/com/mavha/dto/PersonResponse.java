package com.mavha.dto;

import lombok.Data;

@Data
public class PersonResponse {
	private Long id;
	private String name;
	private String lastName;
	private Integer age;
}

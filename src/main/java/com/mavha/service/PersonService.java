package com.mavha.service;

import com.mavha.model.Person;

import java.util.List;

public interface PersonService {

	List<Person> getAll();

	Person create(Person person);

}
